﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TTPP
{

        public class FlyingEnemy : Enemy
        {
            public int batHealth = 10;
            public int dragonStamina = 10;

            public float batSpeed = 0.7f;
            public float batAtkdmg = 1.5f;

            public string batName = "Bat";
            public string batElement = "Shadow";
            public string batText = "Peep Peep";

            public bool batAlive = true;
            public bool batAura = true;
            public bool batFlying = true;
            public bool batDeath;


        }
    }

